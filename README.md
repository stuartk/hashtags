# Hashtag Viewer

Welcome to the hastag viewer.

This tool reads documents and strings of text, extracts the most common words,
records the documents they are used in, and records the setences the words are
found in.

## Running the code

You can run the tool either from the command line, or in a django application.


### From the command line

The main file that does the wordcount is in `core/wordcount.py`. It will load in the 
documents stored in the `core/data` directory.

To run the script:

`python core/wordcount.py`


### From the django application

First install django from the requirements file using pip:

`pip install -r requirements.txt`

Then run the django server:

`python manage.py runserver`

Go to http://localhost:8000, and follow the on screen instructions.

Database migrations are not required as we are not using the database here.


## Configuration

The script/application comes with some default configurations that can be changed.

### Stop words

These are words that we don't want to count, like the, and etc.  The file to change is 
`core/constants.py`.  You can add or amend words in the `STOP_WORDS` list.

### Replace map

This is a map of characters and their replacement.  It's used for cleaning the document and 
removing certain characters.  This again is in `core/constants.py`, and is in the dictionary
named `REPLACE_MAP`

### Top x words

In the django application, the `core/views.py` file contains a `ROWS_TO_DISPLAY` variable.
This is set to 10 currently, and sets the top 10 most common words in the documents.

### Sentences to display

Because some of the words appear in many sentences, we can limit how many sentences to display.
In the `core/views.py` file change the `SENTENCES_TO_DISPLAY` variable.

