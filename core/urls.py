from django.urls import path

from .views import home, view_data

urlpatterns = [path("", home, name="home"), path("data/", view_data, name="view_data")]

