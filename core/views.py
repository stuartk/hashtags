from django.shortcuts import render
from itertools import islice
from .wordcount import WordCount


ROWS_TO_DISPLAY = 10
SENTENCES_TO_DISPLAY = 3


def home(request):
    """ Home page view """
    return render(request, "home.html", {})


def view_data(request):
    """ View the hastag/wordcount data """
    wc = WordCount(
        ["doc1.txt", "doc2.txt", "doc3.txt", "doc4.txt", "doc5.txt", "doc6.txt"]
    )
    wc.process_docs()
    data = list(islice(wc.ordered_map, 0, ROWS_TO_DISPLAY))
    return render(
        request,
        "data.html",
        {
            "words": data,
            "num_records": len(data),
            "num_sentences": SENTENCES_TO_DISPLAY,
        },
    )
