from collections import OrderedDict
from itertools import islice
import os

try:
    from .constants import STOP_WORDS, REPLACE_MAP
except ImportError:
    from constants import STOP_WORDS, REPLACE_MAP

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


class WordCount:
    """
    Processes multiple documents to produce data on the most common words,
    the documents they appear in, and the sentences they are used in.
    """

    def __init__(self, doc_list):
        """
        :param doc_list: A list of filenames in the data directory, or a list of documents as strings
        """
        self.doc_list = doc_list
        self.word_map = OrderedDict()
        self.ordered_map = None

    def process_docs(self):
        """
        Iterates over the doc list, checks if they exist as a file or
        should be processed as a document itself.
        """
        for doc_num, document in enumerate(self.doc_list):
            try:
                path = os.path.join(BASE_DIR, "core/data", document)
                fp = open(path, "r")
                doc = fp.read()
                doc_name = document
            except FileNotFoundError:
                doc = document
                doc_name = doc_num + 1

            self.word_data(doc, doc_name)

        self.ordered_map = sorted(
            self.word_map.items(), key=lambda x: x[1]["count"], reverse=True
        )

    def word_data(self, doc, doc_name):
        """
        Cleans the data in the document, splits it into sentences.
        Then splits into words, adds them to the word map dict.

        :param doc: document as a string
        :param doc_name: the name of the document
        """
        # remove new lines, punctuation characters etc
        for char, replace in REPLACE_MAP.items():
            doc = doc.replace(char, replace)

        # split on sentences, assuming sentences end with a full stop
        sentences = doc.split(".")

        # remove empty sentences, convert to lower and remove trailing/leading spaces
        sentences = [s.lower().strip() for s in sentences if s]

        for sentence in sentences:
            words = sentence.split()
            for word in words:
                # STOP_WORDS are words to be excluded
                if word in STOP_WORDS:
                    continue

                self.word_map.setdefault(
                    word, {"count": 0, "docs": [], "sentences": []}
                )

                wm = self.word_map[word]
                wm["count"] += 1

                if doc_name not in wm["docs"]:
                    self.word_map[word]["docs"].append(doc_name)

                if sentence not in wm["sentences"]:
                    self.word_map[word]["sentences"].append(sentence)


if __name__ == "__main__":
    wc = WordCount(
        ["doc1.txt", "doc2.txt", "doc3.txt", "doc4.txt", "doc5.txt", "doc6.txt"]
    )
    wc.process_docs()
    top_10 = list(islice(wc.ordered_map, 0, 10))
    for k, v in top_10:
        print(k, v["count"], ",".join(v["docs"]), f'"{v["sentences"][0]}"')

